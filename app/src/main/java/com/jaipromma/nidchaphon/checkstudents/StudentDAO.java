package com.jaipromma.nidchaphon.checkstudents;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Nidchaphon on 7/6/16 AD.
 */
public class StudentDAO {
    private SQLiteDatabase database;
    private DbHelper dbHelper;

    public StudentDAO(Context context) {
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public ArrayList<Student> findAll(){
        ArrayList<Student> students = new ArrayList<>();
        String sqlText = "SELECT * FROM student ORDER BY std_id ASC;";
        Cursor cursor =database.rawQuery(sqlText,null);
        cursor.moveToFirst();
        Student student;
        while (!cursor.isAfterLast()){
            student = new Student();
            student.setStdId(cursor.getString(0));
            student.setStdName(cursor.getString(1));
            student.setStdAddress(cursor.getString(2));
            student.setStdTel(cursor.getString(3));
            student.setStdEmail(cursor.getString(4));
            student.setStdPrName(cursor.getString(5));
            student.setStdPrEmail(cursor.getString(6));
            student.setStdPrTel(cursor.getString(7));

            students.add(student);

            cursor.moveToNext();
        }
        return students;
    }


    public boolean add (Student student){
        Student newStudent = student;
        boolean resultNewStudent = false;
        ContentValues values = new ContentValues();
        values.put("std_id", student.getStdId());
        values.put("std_name", student.getStdName());
        values.put("std_address", student.getStdAddress());
        values.put("std_tel", student.getStdTel());
        values.put("std_email", student.getStdEmail());
        values.put("std_pr_name", student.getStdPrName());
        values.put("std_pr_tel", student.getStdPrTel());
        values.put("std_pr_email", student.getStdPrEmail());
        this.database.insert("student", null, values);

        return true;
    }

    public boolean edit(Student student){
        Student editStudent = student;
        boolean resultEditStudent = false;
        ContentValues values = new ContentValues();
        values.put("std_id", student.getStdId());
        values.put("std_name", student.getStdName());
        values.put("std_address", student.getStdAddress());
        values.put("std_tel", student.getStdTel());
        values.put("std_email", student.getStdEmail());
        values.put("std_pr_name", student.getStdPrName());
        values.put("std_pr_tel", student.getStdPrTel());
        values.put("std_pr_email", student.getStdPrEmail());
        String where = "std_id='"+student.getStdId()+"'";
        this.database.update("student", values, where, null);

        return true;
    }

    public void delete(String stdId){
        String sqlDelete = "DELETE FROM student WHERE std_id='"+stdId+"'";
        this.database.execSQL(sqlDelete);
    }
}

package com.jaipromma.nidchaphon.checkstudents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddStdActivity extends AppCompatActivity {

    private EditText stdId, stdName, stdAddress, stdTel, stdEmail, prName, prTel, prEmail;
    private Button btnadd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_std);

        stdId = (EditText) findViewById(R.id.edittext_stdid);
        stdName = (EditText) findViewById(R.id.edittext_stdname);
        stdAddress = (EditText) findViewById(R.id.edittext_stdaddress);
        stdTel = (EditText) findViewById(R.id.edittext_stdtel);
        stdEmail = (EditText) findViewById(R.id.edittext_stdemail);
        prName = (EditText) findViewById(R.id.edittext_prname);
        prTel = (EditText) findViewById(R.id.edittext_prtel);
        prEmail = (EditText) findViewById(R.id.edittext_premail);

        btnadd = (Button) findViewById(R.id.button_add);

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Student student = new Student();
                student.setStdId(String.valueOf(stdId.getText()));
                student.setStdName(String.valueOf(stdName.getText()));
                student.setStdAddress(String.valueOf(stdAddress.getText()));
                student.setStdTel(String.valueOf(stdTel.getText()));
                student.setStdEmail(String.valueOf(stdEmail.getText()));
                student.setStdPrName(String.valueOf(prName.getText()));
                student.setStdPrTel(String.valueOf(prTel.getText()));
                student.setStdPrEmail(String.valueOf(prEmail.getText()));

                StudentDAO studentDAO = new StudentDAO(AddStdActivity.this);
                if (studentDAO.add(student)) {
                    Toast.makeText(AddStdActivity.this,"เพิ่มข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                    finish();
                }

                else
                    Toast.makeText(AddStdActivity.this,"เพิ่มข้อมูลผิดพลาด!!!", Toast.LENGTH_SHORT).show();

            }
        });


    }
}

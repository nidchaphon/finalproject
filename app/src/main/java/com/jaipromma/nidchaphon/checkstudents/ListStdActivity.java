package com.jaipromma.nidchaphon.checkstudents;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ListStdActivity extends AppCompatActivity {

    private ListView stdListview;
    private Student student;

    String[] menuItem = {"ข้อมูลนักศึกษา","แก้ไขข้อมูล","ลบข้อมูล"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_std);

        stdListview = (ListView) findViewById(R.id.listView_std);

    }

    @Override
    protected void onResume() {
        super.onResume();

        StudentDAO studentDAO = new StudentDAO(this);

        final StudentListAdapter adapter = new StudentListAdapter(this, studentDAO.findAll());
        stdListview.setAdapter(adapter);
        studentDAO.close();


        stdListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ListStdActivity.this);
                builder.setTitle("จัดการข้อมูล");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setItems(menuItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String selected = menuItem[which];

                        if (selected==menuItem[0]) {
                            Intent detailStdIntent = new Intent(ListStdActivity.this, DetailStdActivity.class);
                detailStdIntent.putExtra("studentObj", adapter.getItem(position));
                startActivity(detailStdIntent);

                        }

                        else if (selected==menuItem[1]) {
                            Intent editStdIntent = new Intent(ListStdActivity.this, EditStdActivity.class);
                            editStdIntent.putExtra("studentObj", adapter.getItem(position));
                            startActivity(editStdIntent);
                        }

                        else if (selected==menuItem[2]) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(ListStdActivity.this);
                            builder1.setMessage("ต้องการลบนักศึกษาชื่อ "+adapter.getItem(position).getStdName().toString()+" หรือไม่");
                            builder1.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    Toast.makeText(getApplicationContext(),"ลบข้อมูลเรียบร้อย",Toast.LENGTH_SHORT).show();
                                    StudentDAO studentDAO = new StudentDAO(getApplicationContext());
                                    studentDAO.delete(adapter.getItem(position).getStdId());

                                    finish();

                                    Intent openListStd = new Intent(ListStdActivity.this, ListStdActivity.class);
                                    startActivity(openListStd);

                                }
                            });
                            builder1.setNegativeButton("ไม่ใช่", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            builder1.show();
                        }
                    }
                });
                builder.create();
                builder.show();

            }
        });

    }
}

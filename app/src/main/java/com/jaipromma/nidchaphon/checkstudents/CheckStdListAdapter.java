package com.jaipromma.nidchaphon.checkstudents;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nidchaphon on 8/3/16 AD.
 */


public class CheckStdListAdapter extends BaseAdapter {

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Student> students;

    public CheckStdListAdapter(Activity activity, ArrayList<Student> students) {

        this.activity = activity;
        this.students = students;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Student getItem(int position) {
        return this.students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.students.get(position).getStdId());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        view = layoutInflater.inflate(R.layout.listview_check_std, null);
        TextView id = (TextView) view.findViewById(R.id.textView_check_id);
        TextView name = (TextView) view.findViewById(R.id.textView_check_name);
        TextView email = (TextView) view.findViewById(R.id.textView_check_email);
        TextView phone = (TextView) view.findViewById(R.id.textView_check_phone);
        TextView prname = (TextView) view.findViewById(R.id.textView_check_prname);
        CheckBox check = (CheckBox) view.findViewById(R.id.checkBox_check_std);

        id.setText(this.students.get(position).getStdId());
        name.setText(this.students.get(position).getStdName());
        email.setText(this.students.get(position).getStdPrEmail());
        email.setVisibility(View.GONE);
        phone.setText(this.students.get(position).getStdPrTel());
        phone.setVisibility(View.GONE);
        prname.setText(this.students.get(position).getStdPrName());
        prname.setVisibility(View.GONE);
        check.setTag(this.students.get(position).getStdId());

        return view;

    }

}

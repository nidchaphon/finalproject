package com.jaipromma.nidchaphon.checkstudents;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Nidchaphon on 7/6/16 AD.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String databaseName = "checkstudent.sqlite";
    private static final int databaseVersion = 2;
    private Context myContext;

    public DbHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQLText = "CREATE TABLE checkstd (" +
                "ch_id TEXT PRIMARY KEY," +
                "ch_status TEXT," +
                "ch_date TEXT," +
                "std_id TEXT," +
                "std_name TEXT);";
        db.execSQL(SQLText);

        SQLText = "CREATE TABLE student (" +
                "std_id TEXT PRIMARY KEY," +
                "std_name TEXT," +
                "std_address TEXT," +
                "std_tel TEXT," +
                "std_email TEXT," +
                "std_pr_name TEXT," +
                "std_pr_email TEXT," +
                "std_pr_tel TEXT);";
        db.execSQL(SQLText);



//        SQLText = "INSERT INTO student (std_id, std_name, std_address, std_tel, std_email, std_pr_name, std_pr_email, std_pr_tel)" +
//                "VALUES ('5949010101', 'ณัฐภัทร หลีเกี๊ยะ', 'เชียงใหม่', '0865745678', 'maruko@gmail.com', 'สมพงษ์ หลีเกี๊ยะ', 'nut_amd@hotmail.com', '0835485695');";
//        db.execSQL(SQLText);
//
//        SQLText = "INSERT INTO student (std_id, std_name, std_address, std_tel, std_email, std_pr_name, std_pr_email, std_pr_tel)" +
//                "VALUES ('5949010102', 'ณิชพน ใจพรมมา', 'แพร่', '0852689363', 'bill_jakkrit@hotmail.com', 'อุดม ใจพรมมา', 'jakkrit2939@gmail.com', '0805004586');";
//        db.execSQL(SQLText);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sqlText = "DROP TABLE IF EXISTS student;";
        db.execSQL(sqlText);

        sqlText = "DROP TABLE IF EXISTS checkstd;";
        db.execSQL(sqlText);

        onCreate(db);

    }
}

package com.jaipromma.nidchaphon.checkstudents;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nidchaphon on 7/6/16 AD.
 */
public class StudentListAdapter extends BaseAdapter {

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Student> students;

    public StudentListAdapter(Activity activity, ArrayList<Student> students) {
        this.activity = activity;
        this.students = students;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Student getItem(int position) {
        return this.students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.students.get(position).getStdId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        view = layoutInflater.inflate(R.layout.listview_std, null);
        TextView id = (TextView) view.findViewById(R.id.textView_id);
        TextView name = (TextView) view.findViewById(R.id.textView_name);
//        TextView address = (TextView) view.findViewById(R.id.textView_address);
//        TextView tel = (TextView) view.findViewById(R.id.textView_tel);
//        TextView email = (TextView) view.findViewById(R.id.textView_email);

        id.setText(this.students.get(position).getStdId());
        name.setText(this.students.get(position).getStdName());
//        address.setText(this.students.get(position).getStdAddress());
//        tel.setText(this.students.get(position).getStdTel());
//        email.setText(this.students.get(position).getStdEmail());


        return view;
    }
}

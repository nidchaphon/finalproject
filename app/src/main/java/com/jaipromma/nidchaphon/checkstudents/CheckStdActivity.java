package com.jaipromma.nidchaphon.checkstudents;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class CheckStdActivity extends AppCompatActivity {

    private ListView listViewCheckStd;
    private Button btnCheckAll, btnCheckClear, btnSaveCheck;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_std);

        listViewCheckStd = (ListView) findViewById(R.id.listView_checkstd);
        btnCheckAll = (Button) findViewById(R.id.button_checkall);
        btnCheckClear = (Button) findViewById(R.id.button_checkclear);
        btnSaveCheck = (Button) findViewById(R.id.button_save_check);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        final String date = df.format(c.getTime());

        StudentDAO studentDAO = new StudentDAO(this);

        final CheckStdListAdapter adapter = new CheckStdListAdapter(this, studentDAO.findAll());
        listViewCheckStd.setAdapter(adapter);
        studentDAO.close();

        btnCheckAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count = listViewCheckStd.getAdapter().getCount();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout) listViewCheckStd.getChildAt(i);
                    CheckBox checkBox = (CheckBox) itemLayout.findViewById(R.id.checkBox_check_std);
                    checkBox.setChecked(true);
                }

            }
        });

        btnCheckClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count = listViewCheckStd.getAdapter().getCount();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout) listViewCheckStd.getChildAt(i);
                    CheckBox checkBox = (CheckBox) itemLayout.findViewById(R.id.checkBox_check_std);
                    checkBox.setChecked(false);
                }

            }
        });


        btnSaveCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        int count = listViewCheckStd.getAdapter().getCount();
                        for (int i = 0; i < count; i++) {
                            LinearLayout itemLayout = (LinearLayout) listViewCheckStd.getChildAt(i);
                            CheckBox checkBox = (CheckBox) itemLayout.findViewById(R.id.checkBox_check_std);
                            TextView textviewName = (TextView) itemLayout.findViewById(R.id.textView_check_name);
                            TextView textviewEmail = (TextView) itemLayout.findViewById(R.id.textView_check_email);
                            TextView textviewPrName = (TextView) itemLayout.findViewById(R.id.textView_check_prname);
                            TextView textviewPhone = (TextView) itemLayout.findViewById(R.id.textView_check_phone);

                            String email = textviewEmail.getText().toString();
                            String phone = textviewPhone.getText().toString();
                            String subject = String.valueOf("แจ้งเตือนการขาดเรียนของนักศึกษาในความปกครองของท่าน");
                            String attachment = String.valueOf("เรียนคุณ "+textviewPrName.getText().toString()+"\n\n     เนื่องจากนักศึกษาในความปกครองของท่าน ชื่อ"+textviewName.getText().toString()+" <ขาดเรียน> \n     ในวันที่ "+date+" \n\nจึงเรียนมาเพื่อทราบ");

                            if (checkBox.isChecked()) {

                                Checkstd checkstd = new Checkstd();

                                checkstd.setCh_status(String.valueOf("1"));
                                checkstd.setCh_date(String.valueOf(date));
                                checkstd.setStd_id(checkBox.getTag().toString());
                                checkstd.setStd_name(textviewName.getText().toString());

                                CheckStdDAO checkStdDAO = new CheckStdDAO(getApplicationContext());
                                checkStdDAO.checkadd(checkstd);

                            }

                            else {

                                Checkstd checkstd = new Checkstd();

                                checkstd.setCh_status(String.valueOf("0"));
                                checkstd.setCh_date(String.valueOf(date));
                                checkstd.setStd_id(checkBox.getTag().toString());
                                checkstd.setStd_name(textviewName.getText().toString());

                                Intent sendEmail = new Intent(Intent.ACTION_SEND);
                                sendEmail.setType("text/email");
                                sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[] {email.toString()});
                                sendEmail.putExtra(Intent.EXTRA_SUBJECT, subject.toString());
                                sendEmail.putExtra(Intent.EXTRA_TEXT, attachment.toString());
                                startActivity(Intent.createChooser(sendEmail, "Send email with"));

                                CheckStdDAO checkStdDAO = new CheckStdDAO(getApplicationContext());
                                checkStdDAO.checkadd(checkstd);

                            }
                        }
                        Toast.makeText(CheckStdActivity.this,"บันทึกข้อมูลเรียบร้อย", Toast.LENGTH_SHORT).show();
                        finish();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();



    }
}

package com.jaipromma.nidchaphon.checkstudents;

import java.io.Serializable;

/**
 * Created by Nidchaphon on 7/6/16 AD.
 */
public class Student implements Serializable {
    private String stdId;
    private String stdName;
    private String stdAddress;
    private String stdTel;
    private String stdEmail;
    private String stdPrName;
    private String stdPrEmail;
    private String stdPrTel;

    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getStdAddress() {
        return stdAddress;
    }

    public void setStdAddress(String stdAddress) {
        this.stdAddress = stdAddress;
    }

    public String getStdTel() {
        return stdTel;
    }

    public void setStdTel(String stdTel) {
        this.stdTel = stdTel;
    }

    public String getStdEmail() {
        return stdEmail;
    }

    public void setStdEmail(String stdEmail) {
        this.stdEmail = stdEmail;
    }

    public String getStdPrName() {
        return stdPrName;
    }

    public void setStdPrName(String stdPrName) {
        this.stdPrName = stdPrName;
    }

    public String getStdPrEmail() {
        return stdPrEmail;
    }

    public void setStdPrEmail(String stdPrEmail) {
        this.stdPrEmail = stdPrEmail;
    }

    public String getStdPrTel() {
        return stdPrTel;
    }

    public void setStdPrTel(String stdPrTel) {
        this.stdPrTel = stdPrTel;
    }
}

package com.jaipromma.nidchaphon.checkstudents;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Nidchaphon on 8/3/16 AD.
 */
public class CheckStdDAO {

    private SQLiteDatabase database;
    private DbHelper dbHelper;

    public CheckStdDAO (Context context) {
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public ArrayList<Checkstd> reportCheck() {
        ArrayList<Checkstd> checkstds = new ArrayList<>();
        String sqlText = "SELECT * FROM checkstd ORDER BY ch_date DESC";
        Cursor cursor = database.rawQuery(sqlText,null);
        cursor.moveToFirst();
        Checkstd checkstd;
        while (!cursor.isAfterLast()){
            checkstd = new Checkstd();
            checkstd.setCh_id(cursor.getString(0));
            checkstd.setCh_status(cursor.getString(1));
            checkstd.setCh_date(cursor.getString(2));
            checkstd.setStd_id(cursor.getString(3));
            checkstd.setStd_name(cursor.getString(4));

            checkstds.add(checkstd);

            cursor.moveToNext();

        }
        return checkstds;
    }

    public void checkadd (Checkstd checkstd){
        Checkstd addCheck = checkstd;
        ContentValues values = new ContentValues();
        values.put("ch_status", checkstd.getCh_status());
        values.put("ch_date", checkstd.getCh_date());
        values.put("std_id", checkstd.getStd_id());
        values.put("std_name", checkstd.getStd_name());
        this.database.insert("checkstd", null, values);

        Log.d("CheckAdd:::","OK");

    }

}

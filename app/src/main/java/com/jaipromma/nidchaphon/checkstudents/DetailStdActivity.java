package com.jaipromma.nidchaphon.checkstudents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailStdActivity extends AppCompatActivity {

    private TextView stdId, stdName, stdAddress, stdTel, stdEmail, prName, prTel, prEmail;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_std);


        stdId = (TextView) findViewById(R.id.textView_stdid);
        stdName = (TextView) findViewById(R.id.textView_stdname);
        stdAddress = (TextView) findViewById(R.id.textView_stdaddress);
        stdTel = (TextView) findViewById(R.id.textView_stdtel);
        stdEmail = (TextView) findViewById(R.id.textView_stdemail);
        prName = (TextView) findViewById(R.id.textView_prname);
        prTel = (TextView) findViewById(R.id.textView_prtel);
        prEmail = (TextView) findViewById(R.id.textView_premail);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        student = (Student) intent.getSerializableExtra("studentObj");

        stdId.setText(student.getStdId());
        stdName.setText(student.getStdName());
        stdAddress.setText(student.getStdAddress());
        stdTel.setText(student.getStdTel());
        stdEmail.setText(student.getStdEmail());

        prName.setText(student.getStdPrName());
        prEmail.setText(student.getStdPrEmail());
        prTel.setText(student.getStdPrTel());


    }
}

package com.jaipromma.nidchaphon.checkstudents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class CheckStdReportActivity extends AppCompatActivity {

    private ListView listViewCheckReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_std_report);

        listViewCheckReport = (ListView) findViewById(R.id.listView_checkstd_report);

        CheckStdDAO checkStdDAO = new CheckStdDAO(this);
        final CheckStdReportListAdapter adapter = new CheckStdReportListAdapter(this, checkStdDAO.reportCheck());
        listViewCheckReport.setAdapter(adapter);
        checkStdDAO.close();

    }
}

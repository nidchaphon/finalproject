package com.jaipromma.nidchaphon.checkstudents;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nidchaphon on 8/3/16 AD.
 */
public class CheckStdReportListAdapter extends BaseAdapter {

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Checkstd> checkstds;
    private Student student;



    public CheckStdReportListAdapter(Activity activity, ArrayList<Checkstd> checkstds) {
        this.activity = activity;
        this.checkstds = checkstds;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return checkstds.size();
    }

    @Override
    public Checkstd getItem(int position) {
        return this.checkstds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.checkstds.get(position).getCh_id());
    }

    private Integer statusdata;
    private String namedata;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        view = layoutInflater.inflate(R.layout.listview_check_report, null);
        TextView id = (TextView) view.findViewById(R.id.textView_report_id);
        TextView name = (TextView) view.findViewById(R.id.textView_report_name);
        TextView status = (TextView) view.findViewById(R.id.textView_report_status);
        TextView date = (TextView) view.findViewById(R.id.textView_report_date);

        statusdata = Integer.valueOf(this.checkstds.get(position).getCh_status());

            if (statusdata==1) {
                status.setText(String.valueOf("มา"));
            }
            else if (statusdata==0) {
                status.setText(String.valueOf("ขาด"));
            }

        id.setText(this.checkstds.get(position).getStd_id());
        name.setText(this.checkstds.get(position).getStd_name());
        date.setText(this.checkstds.get(position).getCh_date());

        return view;
    }
}

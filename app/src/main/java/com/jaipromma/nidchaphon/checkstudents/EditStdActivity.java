package com.jaipromma.nidchaphon.checkstudents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditStdActivity extends AppCompatActivity {

    private TextView stdId;
    private EditText stdName, stdAddress, stdTel, stdEmail, prName, prTel, prEmail;
    private Button btnEdit;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_std);

        Intent intent = getIntent();
        student = (Student) intent.getSerializableExtra("studentObj");

        stdId = (TextView) findViewById(R.id.textview_stdid);
        stdName = (EditText) findViewById(R.id.edittext_stdname);
        stdAddress =(EditText) findViewById(R.id.edittext_stdaddress);
        stdTel = (EditText) findViewById(R.id.edittext_stdtel);
        stdEmail = (EditText) findViewById(R.id.edittext_stdemail);
        prName = (EditText) findViewById(R.id.edittext_prname);
        prTel = (EditText) findViewById(R.id.edittext_prtel);
        prEmail = (EditText) findViewById(R.id.edittext_premail);

        stdId.setText(student.getStdId());
        stdName.setText(student.getStdName());
        stdAddress.setText(student.getStdAddress());
        stdTel.setText(student.getStdTel());
        stdEmail.setText(student.getStdEmail());
        prName.setText(student.getStdPrName());
        prTel.setText(student.getStdPrTel());
        prEmail.setText(student.getStdPrEmail());

        btnEdit = (Button) findViewById(R.id.button_edit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StudentDAO studentDAO = new StudentDAO(getApplicationContext());
                Student editStudent = new Student();
                editStudent.setStdId(student.getStdId());
                editStudent.setStdName(String.valueOf(stdName.getText()));
                editStudent.setStdAddress(String.valueOf(stdAddress.getText()));
                editStudent.setStdTel(String.valueOf(stdTel.getText()));
                editStudent.setStdEmail(String.valueOf(stdEmail.getText()));
                editStudent.setStdPrName(String.valueOf(prName.getText()));
                editStudent.setStdPrTel(String.valueOf(prTel.getText()));
                editStudent.setStdPrEmail(String.valueOf(prEmail.getText()));

                if (studentDAO.edit(editStudent)){
                    Toast.makeText(getApplicationContext(), "แก้ไขสำเร็จ", Toast.LENGTH_SHORT).show();

                    finish();
                }
                else
                    Toast.makeText(getApplicationContext(), "แก้ไขผิดพลาด!!!", Toast.LENGTH_SHORT).show();


            }
        });

    }
}

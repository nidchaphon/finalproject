package com.jaipromma.nidchaphon.checkstudents;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnListStd, btnCheckStd, btnReportCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                Intent addStdActivity = new Intent(getApplicationContext(), AddStdActivity.class);
                startActivity(addStdActivity);


            }
        });

        btnListStd = (Button) findViewById(R.id.btn_liststd);
        btnCheckStd = (Button) findViewById(R.id.btn_checkstd);
        btnReportCheck = (Button) findViewById(R.id.btn_report);


        btnListStd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listStdPage = new Intent(MainActivity.this, ListStdActivity.class);
                startActivity(listStdPage);

            }
        });

        btnCheckStd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent checkStdPage = new Intent(MainActivity.this, CheckStdActivity.class);
//                Intent checkStdPage = new Intent(MainActivity.this, CheckAddStdActivity.class);
                startActivity(checkStdPage);

            }
        });

        btnReportCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent reportCheckPage = new Intent(MainActivity.this, CheckStdReportActivity.class);
                startActivity(reportCheckPage);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {

            Intent infoPage = new Intent(getApplicationContext(), InfoMyAppActivity.class);
            startActivity(infoPage);

            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}

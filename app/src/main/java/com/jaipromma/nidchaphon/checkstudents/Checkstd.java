package com.jaipromma.nidchaphon.checkstudents;

/**
 * Created by Nidchaphon on 7/6/16 AD.
 */
public class Checkstd {
    private String ch_id;
    private String ch_status;
    private String ch_date;
    private String std_id;
    private String std_name;

    public String getCh_id() {
        return ch_id;
    }

    public void setCh_id(String ch_id) {
        this.ch_id = ch_id;
    }

    public String getCh_status() {
        return ch_status;
    }

    public void setCh_status(String ch_status) {
        this.ch_status = ch_status;
    }

    public String getCh_date() {
        return ch_date;
    }

    public void setCh_date(String ch_date) {
        this.ch_date = ch_date;
    }

    public String getStd_id() {
        return std_id;
    }

    public void setStd_id(String std_id) {
        this.std_id = std_id;
    }

    public String getStd_name() {
        return std_name;
    }

    public void setStd_name(String std_name) {
        this.std_name = std_name;
    }
}
